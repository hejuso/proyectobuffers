package view;

import java.awt.Dimension;

import javax.swing.*;

public class LaunchView extends JFrame {

	private JButton comparar, buscar, copiar, btnGuardarLibro, btnMostrarLibros, btnEncontrarLibro, btnActualizar,
			btnModificarLibro;
	private JTextArea textArea;
	private JTextField fichero1, fichero2, palabra, origen, destino;
	private JLabel label_f1, label_f2, label_pal, label_dest1, label_dest2;
	private JCheckBox primera;

	private JPanel panel;
	
	private JTextField idLibro;

	private JTextField textFieldTitulo;

	private JLabel idLibroLabel;

	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;

	private JTextField textFieldPubli;
	private JLabel lblNewLabel_3;
	private JTextField textFieldEditor;
	private JLabel lblNewLabel_4;
	private JTextField textFieldPaginas;
	private JTextField textFieldAutor;
	private JTextField nombreLibro;

	public LaunchView() {

		setBounds(200, 200, 900, 500);
		setTitle("Proyecto Buffers");
		panel = new JPanel();

		comparar = new JButton("Comparar contenido");
		comparar.setPreferredSize(new Dimension(150, 26));
		buscar = new JButton("Buscar palabra");
		buscar.setPreferredSize(new Dimension(150, 26));

		// COPIA DE FICHERO

		copiar = new JButton("Copiar el fichero");
		copiar.setPreferredSize(new Dimension(150, 26));

		label_dest1 = new JLabel("Origen: ");
		origen = new JTextField("", 10);
		label_dest2 = new JLabel("Destino: ");
		destino = new JTextField("", 10);

		fichero1 = new JTextField("", 10);
		fichero2 = new JTextField("", 10);
		palabra = new JTextField("", 10);

		label_f1 = new JLabel("Fichero 1:");
		label_f2 = new JLabel("Fichero 2:");
		label_pal = new JLabel("Palabra:");

		primera = new JCheckBox("Primera aparici�n");

		textArea = new JTextArea(20, 80);
		textArea.setBounds(50, 50, 50, 50);
		textArea.setEditable(false);

		panel.add(comparar);
		panel.add(buscar);
		panel.add(label_f1);
		panel.add(fichero1);
		panel.add(label_f2);
		panel.add(fichero2);
		panel.add(label_pal);
		panel.add(palabra);
		panel.add(primera);
		panel.add(copiar);
		panel.add(label_dest1);
		panel.add(origen);
		panel.add(label_dest2);
		panel.add(destino);

		panel.add(textArea);

		//LIBROS
		
		// ID LIBRO
		idLibroLabel = new JLabel("ID");
		panel.add(idLibroLabel);
		idLibro = new JTextField();
		panel.add(idLibro);
		idLibro.setColumns(3);

		// Titulo LIBRO
		lblNewLabel = new JLabel("T\u00EDtulo");
		panel.add(lblNewLabel);
		textFieldTitulo = new JTextField();
		panel.add(textFieldTitulo);
		textFieldTitulo.setColumns(10);

		// AUTOR LIBRO
		lblNewLabel_1 = new JLabel("Autor");
		panel.add(lblNewLabel_1);
		textFieldAutor = new JTextField();
		panel.add(textFieldAutor);
		textFieldAutor.setColumns(10);

		// A�O LIBRO
		lblNewLabel_2 = new JLabel("A\u00F1o de publicaci\u00F3n");
		panel.add(lblNewLabel_2);
		textFieldPubli = new JTextField();
		panel.add(textFieldPubli);
		textFieldPubli.setColumns(5);

		// EDITOR LIBRO
		lblNewLabel_3 = new JLabel("Editor");
		panel.add(lblNewLabel_3);
		textFieldEditor = new JTextField();
		panel.add(textFieldEditor);
		textFieldEditor.setColumns(10);

		// NUM PAGINAS LIBRO
		lblNewLabel_4 = new JLabel("N\u00BA p\u00E1ginas");
		panel.add(lblNewLabel_4);
		textFieldPaginas = new JTextField();
		panel.add(textFieldPaginas);
		textFieldPaginas.setColumns(3);

		// GUARDAR LIBRO
		btnGuardarLibro = new JButton("Guardar libro");
		panel.add(btnGuardarLibro);

		// RECUPERAR LIBRO
		btnMostrarLibros = new JButton("Mostrar libros");
		panel.add(btnMostrarLibros);

		// ENCONTRAR LIBRO
		btnEncontrarLibro = new JButton("Encontrar libro");
		panel.add(btnEncontrarLibro);
		
		// BUSCAR LIBRO
		nombreLibro = new JTextField();
		panel.add(nombreLibro);
		nombreLibro.setColumns(6);

		// A�adimos el JPanel al JFrame
		this.getContentPane().add(panel);

	}

	public JButton getComparar() {
		return comparar;
	}

	public void setComparar(JButton comparar) {
		this.comparar = comparar;
	}

	public JButton getBuscar() {
		return buscar;
	}

	public void setBuscar(JButton buscar) {
		this.buscar = buscar;
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(JTextArea textArea) {
		this.textArea = textArea;
	}

	public JTextField getFichero1() {
		return fichero1;
	}

	public void setFichero1(JTextField fichero1) {
		this.fichero1 = fichero1;
	}

	public JTextField getFichero2() {
		return fichero2;
	}

	public void setFichero2(JTextField fichero2) {
		this.fichero2 = fichero2;
	}

	// A�adimos el campo get palabra

	public JTextField getPalabra() {
		return palabra;
	}

	// A�adimos el campo getPrimera (checkbox)

	public JCheckBox getPrimera() {
		return primera;
	}

	// GET Y SET ARCHIVOS

	public JTextField getOrigen() {
		return origen;
	}

	public void setOrigen(JTextField origen) {
		this.origen = origen;
	}

	public JTextField getDestino() {
		return destino;
	}

	public void setDestino(JTextField destino) {
		this.destino = destino;
	}

	public JButton getCopiar() {
		return copiar;
	}

	public void getCopiar(JButton copiar) {
		this.copiar = copiar;
	}
	
	// LIBROS
	
	public JTextField getTextFieldId() {
		return idLibro;
	}

	public void getTextFieldId(JTextField idLibro) {
		this.idLibro = idLibro;
	}
	
	public JTextField getTextFieldTitulo() {
		return textFieldTitulo;
	}

	public void setTextFieldTitulo(JTextField textFieldTitulo) {
		this.textFieldTitulo = textFieldTitulo;
	}
	
	public JTextField getTextFieldAutor() {
		return textFieldAutor;
	}

	public void setTextFieldAutor(JTextField textFieldAutor) {
		this.textFieldAutor = textFieldAutor;
	}

	public JTextField getTextFieldPubli() {
		return textFieldPubli;
	}

	public void setTextFieldPubli(JTextField textFieldPubli) {
		this.textFieldPubli = textFieldPubli;
	}

	public JTextField getTextFieldEditor() {
		return textFieldEditor;
	}

	public void setTextFieldEditor(JTextField textFieldEditor) {
		this.textFieldEditor = textFieldEditor;
	}

	
	public JTextField getTextFieldPaginas() {
		return textFieldPaginas;
	}

	public void setTextFieldPaginas(JTextField textFieldPaginas) {
		this.textFieldPaginas = textFieldPaginas;
	}
	
	public JButton getBtnGuardarLibro() {
		return btnGuardarLibro;
	}

	public void setBtnGuardarLibro(JButton btnGuardarLibro) {
		this.btnGuardarLibro = btnGuardarLibro;
	}
	
	public void setBtnMostrarLibros(JButton btnMostrarLibros) {
		this.btnMostrarLibros = btnMostrarLibros;
	}
	
	public JButton getBtnMostrarLibros() {
		return btnMostrarLibros;
	}

	public void btnRecuperarLibros(JButton btnMostrarLibros) {
		this.btnMostrarLibros = btnMostrarLibros;
	}
	
	public JButton getBtnEncontrarLibro() {
		return btnEncontrarLibro;
	}

	public void setBtnEncontrarLibro(JButton btnEncontrarLibro) {
		this.btnEncontrarLibro = btnEncontrarLibro;
	}
	
	public JTextField getTextNombreLibro() {
		return nombreLibro;
	}

	public void setTextFieldNombreLibro(JTextField nombreLibro) {
		this.nombreLibro = nombreLibro;
	}
	
	public void showError(String m) {
		JOptionPane.showMessageDialog(this.panel, m, "Error", JOptionPane.ERROR_MESSAGE);
	}

}
