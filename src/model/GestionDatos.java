package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class GestionDatos {

	public GestionDatos() {

	}

	// TODO: Implementa una funci�n para abrir ficheros
	public BufferedReader abrirFicheros(String file) throws FileNotFoundException {
		return new BufferedReader(new FileReader(file));
	}

	// TODO: Implementa una funci�n para cerrar ficheros
	public void cerrarFicheros(BufferedReader br) throws IOException {
		br.close();
	}

	public boolean compararContenido(String fichero1, String fichero2) throws IOException {
		// Inicializamos un ArrayList en el que guardaremos el contenido de los archivos
		ArrayList<String> contArchivos = new ArrayList<String>();
		// Inicializamos los buffers
		BufferedReader buffer1 = abrirFicheros(fichero1);
		BufferedReader buffer2 = abrirFicheros(fichero2);

		while ((fichero1 = buffer1.readLine()) != null) {
			contArchivos.add(fichero1);
		}
		cerrarFicheros(buffer1);

		while ((fichero2 = buffer2.readLine()) != null) {
			if (contArchivos.contains(fichero2)) {
				return true;
			}
		}
		cerrarFicheros(buffer2);
		return false;
	}

	public int buscarPalabra(String fichero1, String palabra, boolean primera_aparicion) throws IOException {
		int numLinea = 0;
		int ultimaLinea = 0;
		boolean encontrada = false;
		BufferedReader buffer1 = abrirFicheros(fichero1);

		while ((fichero1 = buffer1.readLine()) != null) {
			numLinea++;

			// Si encuentra la palabra y el check est� activado, cierra el buffer y devuelve
			// el numero de la linea
			if (primera_aparicion) {
				if (fichero1.equalsIgnoreCase(palabra)) {
					cerrarFicheros(buffer1);
					return numLinea;
				}
			} else {
				// Si primera aparicion no esta checked, no cerrar� el buffer y devolver� la
				// ultima linea
				if (fichero1.equalsIgnoreCase(palabra)) {
					ultimaLinea = numLinea;
					encontrada = true;
				}
			}
		}
		if (encontrada) {
			return ultimaLinea;
		} else {
			return -1;
		}
	}

	public int copiarContenido(String fichero1, String fichero2) throws IOException {
		// Inicializamos el input / output
		FileInputStream streamInput = new FileInputStream(fichero1);
		FileOutputStream streamOutput = new FileOutputStream(fichero2);
		int tamanyo = 0;

		// Designamos el tipo byte y su peso
		byte[] archivo = new byte[1024];
		int numBytes = 0;

		// Copiamos el archivo
		while ((numBytes = streamInput.read(archivo)) != -1) {
			streamOutput.write(archivo, 0, numBytes);

			// Nos guardamos el tama�o
			tamanyo += numBytes;
		}

		streamInput.close();
		streamOutput.close();

		return tamanyo;

	}

	// LIBROS

	public int guardarLibro(LibroModel libro) throws IOException {

		ObjectOutputStream out = null;

		try {
			out = new ObjectOutputStream(new FileOutputStream("libros/" + libro.getTitulo()));
			out.writeObject(libro);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			out.close();
		}

		return 1;
	}

	public LibroModel leerLibro(String titulo) throws FileNotFoundException, IOException, ClassNotFoundException {

		LibroModel p = null;
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream("libros/" + titulo));

			p = (LibroModel) in.readObject();
		} catch (ClassNotFoundException ex) {
			System.err.println("Error de fichero");
		} catch (IOException ex) {
			System.err.println(ex);
		} finally {
			in.close();
		}
		return p;
	}

	public ArrayList<LibroModel> mostrar_todos() throws FileNotFoundException, IOException, ClassNotFoundException {
		ArrayList<LibroModel> libros = new ArrayList<LibroModel>();
		File f = new File("libros");
		for (File libro : f.listFiles()) {
			FileInputStream fis = new FileInputStream(libro);
			ObjectInputStream ois = new ObjectInputStream(fis);
			libros.add((LibroModel) ois.readObject());
			fis.close();
			ois.close();
		}
		return libros;
	}
}
