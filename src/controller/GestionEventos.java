package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import model.*;
import view.*;

public class GestionEventos {

	private GestionDatos model;
	private LaunchView view;
	private ActionListener actionListener_comparar, actionListener_buscar, actionListener_copiar,
			actionListener_guardar, actionListener_recuperar, actionListener_encontrar, actionListener_modificar;

	public GestionEventos(GestionDatos model, LaunchView view) {
		this.model = model;
		this.view = view;
	}

	public void contol() {
		actionListener_comparar = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				// TODO: Llamar a la funci�n call_compararContenido
				call_compararContenido();
			}
		};
		view.getComparar().addActionListener(actionListener_comparar);

		actionListener_buscar = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				// TODO: Llamar a la funci�n call_buscarPalabra
				call_buscarPalabra();
			}
		};
		view.getBuscar().addActionListener(actionListener_buscar);

		actionListener_copiar = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				// TODO: Llamar a la funci�n call_copiarContenido
				call_copiarContenido();
			}
		};
		view.getCopiar().addActionListener(actionListener_copiar);

		// Libros

		actionListener_guardar = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				// TODO: Llamar a la funci�n call_copiarContenido
				call_guardarLibros();
			}
		};
		view.getBtnGuardarLibro().addActionListener(actionListener_guardar);

		actionListener_recuperar = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				// TODO: Llamar a la funci�n call_copiarContenido
				call_mostrarTodos();
			}
		};
		view.getBtnMostrarLibros().addActionListener(actionListener_recuperar);

		actionListener_encontrar = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				// TODO: Llamar a la funci�n call_copiarContenido
				call_encontrarLibros();
			}
		};
		view.getBtnEncontrarLibro().addActionListener(actionListener_encontrar);

	}

	private int call_compararContenido() {

		try {
			// Si la funci�n compararContenido (seleccionada de el modelo) devuelve true,
			// mostraremos el mensaje de
			// �xito
			if (model.compararContenido(view.getFichero1().getText(), view.getFichero2().getText())) {
				view.getTextArea().setText("Los dos archivos tienen las mismas palabras");
			} else {
				view.getTextArea().setText("Los dos archivos no tienen las mismas palabras");
			}
		} catch (IOException e) {
			view.showError("Ha ocurrido un error, revise los campos");
		}
		return 1;
	}

	private int call_buscarPalabra() {

		int lineaEncontrado;
		try {
			lineaEncontrado = model.buscarPalabra(view.getFichero1().getText(), view.getPalabra().getText(),
					view.getPrimera().isSelected());
			if (lineaEncontrado != -1) {
				view.getTextArea().setText("Se ha encontrado la palabra: " + view.getPalabra().getText()
						+ " en la linea " + lineaEncontrado);
			} else {
				view.showError("La palabra " + view.getPalabra().getText() + " no ha sido encontrada");
			}
		} catch (IOException e) {
			view.showError("Ha ocurrido un error, lo lamentamos");
		}

		return 1;
	}

	private int call_copiarContenido() {

		try {
			int noOfBytes = model.copiarContenido(view.getOrigen().getText(), view.getDestino().getText());
			view.getTextArea().setText("El archivo: " + view.getDestino().getText()
					+ " ha sido generado correctamente \n " + " Se han copiado " + noOfBytes + " Bytes");
		} catch (IOException io) {
			view.showError("Ha ocurrido un error");
		}
		return 1;
	}

	// LIBROS

	private int call_guardarLibros() {

		try {
			if (!view.getTextFieldTitulo().getText().isEmpty() && !view.getTextFieldAutor().getText().isEmpty()
					&& !view.getTextFieldPubli().getText().isEmpty() && !view.getTextFieldEditor().getText().isEmpty()
					&& !view.getTextFieldPaginas().getText().isEmpty()) {

				LibroModel libro = new LibroModel(view.getTextFieldId().getText(), view.getTextFieldTitulo().getText(),
						view.getTextFieldAutor().getText(), view.getTextFieldPubli().getText(),
						view.getTextFieldEditor().getText(), view.getTextFieldPaginas().getText());

				model.guardarLibro(libro);
				view.getTextArea().setText("Libro \"" + view.getTextFieldTitulo().getText() + "\" creado con �xito");

			} else {
				view.showError("Faltan campos por rellenar");
			}
		} catch (Exception e) {
			view.showError("Hubo un problema creando el fichero");
		}

		return 1;
	}

	private int call_encontrarLibros() {

		try {
			System.out.println(view.getTextNombreLibro().getText());
			if (!view.getTextNombreLibro().getText().isEmpty()) {
				LibroModel l = model.leerLibro(view.getTextNombreLibro().getText());
				String s = "Detalles libro: \n\n";
				s += "ID: " + l.getId() + " \n\n";
				s += "T�tulo: " + l.getTitulo() + " \n\n";
				s += "Autor: " + l.getAutor() + " \n\n";
				s += "A�o de publicaci�n: " + l.getPubli() + " \n\n";
				s += "Editor: " + l.getEditor() + " \n\n";
				s += "N� de p�ginas: " + l.getPaginas() + " \n\n";
				view.getTextArea().setText(s);
			} else {
				view.showError("El campo no puede ser vacio");
			}
		} catch (Exception e) {
			view.showError("Hubo un error leyendo el fichero");
		}

		return 1;
	}

	private int call_mostrarTodos() {
		try {
			String s = "Libros: \n";
			int contador = 1;
			ArrayList<LibroModel> libros = model.mostrar_todos();
			if(libros.size() > 0) {
				for(LibroModel libro: libros) {
					s += "Libro n� "+contador+" \n";
					s += "- ID: "+libro.getId()+"\n";
					s += "- Titulo: "+libro.getTitulo()+"\n";
					s += "- Autor: "+libro.getAutor()+"\n";
					s += "- A�o de publicaci�n: "+libro.getPubli()+"\n";
					s += "- Editor: "+libro.getEditor()+"\n";
					s += "- N� de paginas: "+libro.getPaginas()+" \n\n";
					contador++;
				}
				view.getTextArea().setText(s);
				
			} else {
				view.getTextArea().setText("No hay ningún libro");
			}
		} catch (Exception e) {
			view.showError("Hubo un error leyendo los ficheros");
		}
		return 1;
	}

}
